#!/usr/bin/env python

import pickle
import time
import feedparser
import Queue
import threading
import requests

LINKS_FILE = "rss_list"
TS_FILE = "timestamps"

MAX_DL_THREADS = 5

dl_queue = Queue.Queue(MAX_DL_THREADS)

class Downloader(threading.Thread):
    def __init__(self, dl_queue):
        threading.Thread.__init__(self)
        self.dl_queue = dl_queue

    def run(self):
        while True:
            l, f = self.dl_queue.get()

            #print "Downloading " + f + "..."
            
            with open(f, 'wb') as handle:
                request = requests.get(l, stream=True)
                handle.write(request.content)
                
                # for block in request.iter_content(1024):
                #     if not block:
                #         break

                # handle.write(block)
            
            self.dl_queue.task_done()


def main():
    for i in range(MAX_DL_THREADS):
        t = Downloader(dl_queue)
        t.daemon = True
        t.start()

    try:
        with open(TS_FILE, "rb") as f:
            ts = pickle.load(f)
    except IOError:
        ts = {}
    
    with open(LINKS_FILE, "r") as f:
        links = list()

        for line in f:
            tmp = line.split(" ")

            if len(tmp) == 2:
                links.append(tmp[1])
                if not ts.has_key(tmp[1]):
                    ts[tmp[1]] = time.strptime(tmp[0], "%d-%m-%y")
            else:
                links.append(tmp[0])

    for link in links:
        if not ts.has_key(link):
            ts[link] = time.gmtime(0)

        f = feedparser.parse(link)

        for e in reversed(f.entries):
            if ts[link] < e.published_parsed:
                ts[link] = e.published_parsed
                #print "Putting " + e.title + " in the queue"
                dl_queue.put((e.link, e.title))

    with open(TS_FILE, "wb") as f:
        pickle.dump(ts, f)

    dl_queue.join()

if __name__ == "__main__":
    main()
